## Web Parser

Web Parser takes an input URL, an output file name, and the elements you'll want to extract and puts them into a blank file (one element per line).


### Usage
```
ruby parse-web-table.rb {url} {output_file} {parent_element} {content_element}
```

### Example
```
ruby parse-web-table.rb http://www.neimanmarcus.com/Designers/cat000730/c.cat ~/Desktop/test.csv '#designerscolumn' a
```
