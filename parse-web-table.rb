require 'nokogiri'
require 'open-uri'
require 'pry'

class WebParser
  def initialize
    @url = ARGV[0]
    @output_file = ARGV[1]
    @container = ARGV[2]
    @element = ARGV[3]
    @content = ""
    begin_parsing
  end

  def begin_parsing
    get_web_content
    @document.css([@container, @element].compact.join(' ')).each do |element|
      parse_item(element.content.strip.squeeze(" "))
    end
    output_content
  end

  private

  def get_web_content
    @document = Nokogiri::HTML(open(@url))
  end

  def output_content
    File.open(@output_file, 'w+') { |file| file.write(@content) }
  end

  def parse_item(content_item)
    return if content_item.nil? or content_item.empty?
    @content << content_item + "\n"
  end
end

WebParser.new
